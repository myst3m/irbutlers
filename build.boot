(set-env!
 :source-paths #{"src" "test"}
 :resource-paths #{"src" "resources"}
 :repositories {"clojars" "https://clojars.org/repo/"
                "central" "http://repo1.maven.org/maven2/"
                "theorems.co" "https://theorems.co/m2/"}
 :dependencies '[[org.clojure/clojure "1.9.0"]
                 [bottify "0.9.3"
                  :exclusions [org.slf4j/slf4j-log4j12]]
                 [irmagician "0.7.3"]
                 [silvur "1.3.4"]
                 [compojure "1.6.0"]
                 [ring/ring-defaults "0.3.1"]
                 [ring "1.6.3"]
                 [irbutlers/irene-ui "0.9.3"]
                 [org.clojure/data.json "0.2.6"]
                 [org.immutant/web "2.1.9"
                  :exclusions [ch.qos.logback/logback-classic]]
                 [org.clojure/data.codec "0.1.1"]
                 [http-kit "2.2.0"]
                 [org.clojure/core.async "0.3.465"]
                 [adzerk/bootlaces "0.1.13" :scope "test"]
                 [adzerk/boot-test "1.2.0" :scope "test"]])


(require '[adzerk.boot-test :refer :all])
(require '[adzerk.bootlaces :refer :all])
(require '[irbutlers.config :refer [+version+]])


(task-options!
 pom {:project 'irbutlers
      :version +version+})

(deftask build []
  (comp (aot :all true) (pom) (uber)  (jar :main 'irbutlers.core :file (str "irbutlers-" +version+ ".jar" )) (target)))




