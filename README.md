#  An Agent to control irMagician via Google Home

irMagician is the device to control consumer electronics which is controllable with Ir (InfraRed) Remote Control like TV, Air conditioner and so on. It can be purchased from http://www.omiya-giken.com .
This application enables users to control irMagician interactively via Google Home (as of 31.Dec 2017)

Notes: Still Under development

## Installation

Download files and 'boot' which is build tool for clojure. (https://github.com/boot-clj/boot)
And then, execute the following command.

```sh
$ boot build

```

You can find irbutlers-[version].jar in target directory.
(Rename it to irbutlers.jar for below explanation)

irbutlers also has primitive OAuth2 server for private use.
This is expected to work with ngrok.

## Setup

Before booting agent, you need to write your token to a file (ex. irene.secrets).
The token can be used for OAuth2 authorization to identify you.

This token is used as your unique ID.

Finally, you can boot sofia with irMagician mapped to /dev/ttyACM0.
(It may be /dev/ttyACM[1-9],  if you connect some devices using CDC-ACM)

```sh
$ java -jar target/irbutlers.jar -i /dev/ttyACM0 -s irene.secrets irbutlers.irene 

```

This application listens to messages on 10001/tcp as default. You can change that by providing '-p PORT' as argument.

Then, You can access Web UI http://localhost:10001

```console
$ java -jar target/irbutlers-0.4.2-SNAPSHOT.jar -i /dev/ttyACM0 -s irene.secrets irbutlers.irene

17-12-31 12:47:52 galois INFO [irbutlers.irene:202] - Boot:  {:port 10001, :host "0.0.0.0", :admin false, :ir {:path "/dev/ttyACM0"}, :secrets "irene.secrets", :log-level :info}
17-12-31 12:47:52 galois INFO [irbutlers.bianca:245] - Servers ready

```

You can see 'help' by '--help' option.

## License

Copyright © 2016-2018 Tsutomu Miyashita.

Distributed under the Eclipse Public License either version 1.0 or any later version.
