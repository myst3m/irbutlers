(ns irbutlers.util
  (:require [clojure.data.json :as json]
            [clojure.string :as s]
            [clojure.java.io :as io]
            [taoensso.timbre :as log])
  (:require [irmagician.core :refer [set-default-ir-port!  with-ir-port ir-open ir-close! ir-restore ir-play ir-capture ir-save ir-version ir-target-device ir-bank-memory-seq ir-data-length ir-postscale]]))

(defonce success {:status 0})
(defonce fail {:status -1})


(defn json<- [m]
  (try
    (json/write-str m)
    (catch Exception e nil)))

(defn json-> [txt]
  (try
    (json/read-str txt :key-fn keyword)
    (catch Exception e nil)))


(defn device-scan [& dev-regexp]
  (keep identity (map-indexed (fn [i dev]
                                (try (with-ir-port (ir-open {:path (.getPath dev)})
                                       (let [v (ir-version)]
                                         (when (string? v) {:name (str "irMagician-" i) :path (.getPath dev) :did (.getPath dev)})))
                                     (catch Exception e (log/error (.getMessage e)))))
                              (filter #(re-find (re-pattern (or dev-regexp "ttyACM")) (.getPath %))
                                      (file-seq (io/file "/dev"))))))
