(ns irbutlers.irene
  (:require [clojure.data.json :as json]
            [clojure.string :as s]
            [clojure.data.codec.base64 :as b64]
            [org.httpkit.client :as http]
            [compojure.core :refer :all]
            [ring.middleware.defaults :refer [api-defaults wrap-defaults]]
            [ring.middleware.resource :refer (wrap-resource)]
            [taoensso.timbre :as log]
            [irbutlers.util :as u]
            [irbutlers.config :as config]
            [clojure.java.io :as io]
            [clojure.set :as set]
            [irbutlers.oauth :as oauth]
            [irmagician.core :refer [set-default-ir-port!  with-ir-port ir-open ir-close! ir-restore ir-play ir-capture ir-save ir-version ir-target-device ir-bank-memory-seq ir-data-length ir-postscale]]
            [bottify.container :refer [container boot-on-container stop-container]]))


(def supported-types
  {:LIGHT :action.devices.types.LIGHT
   ;; :TV :action.devices.types.LIGHT
   })
(def supported-interfaces
   ;; Own interface name : Action interface
  {:action.devices.traits.OnOff :action.devices.traits.OnOff
   ;; :action.devices.traits.extra.Channel :action.devices.traits.Channel
   })


(def default-did "irmagician-01")
(defonce devices (atom {}))

(defonce options (atom {}))

(defonce actions (atom []))

(defn device [tags]
  (filter #(seq (set/intersection (set (:tags %)) (set (flatten [tags])))) @devices))

(defn devices<- [actions]
  (->> actions
       (map (fn [a] [[(:name a) (:category a) (:controller a)] (supported-interfaces (keyword (-> a :interface :id)))]))
       (filter last)
       (group-by first)
       (map (fn [[k vs]] [k (distinct (map last vs))]))
       (filter (fn [[[name type _] _]] (supported-types (keyword type))) )
       (map (fn [[[name type controller] intfs]]
              {:id (str name "-" type "-" controller)
               :type (supported-types (keyword type))
               :traits (vec intfs)
               :name {:name name}}))))

(defn action [actions name-category cmd-name params]
  (let [[n c ctl] (s/split name-category #"-")
        trait-name (s/replace cmd-name #"commands" "traits")]
    (map #(assoc % :device name-category)
         (filter (fn [{:keys [name category controller interface]}]
                   (and (= [n c ctl] [name category controller])
                        (= (-> interface :id) trait-name)
                        (= (-> interface :params)  params)))
                 actions))))

(defn agent-user-id []
  (s/join (map char (map inc (map long (:ukey @options))))))

(defn handle-action [{:keys [body headers] :as req}]
  (log/debug "Request: " req)
  (let [{:keys [inputs requestId]} (json/read-str (slurp body) :key-fn keyword)
        {:keys [intent payload]} (first inputs)]

    (log/debug "action handler:" intent payload)

    (condp = (keyword intent)
      :action.devices.SYNC (do (log/debug "SYNC: " intent)
                               (log/debug " devices: " (seq (devices<- @actions)))
                               {:requestId (str requestId)
                                :payload {:agentUserId (agent-user-id)
                                          :devices (devices<- @actions)}})
      :action.devices.EXECUTE (let [cmds (flatten
                                          (for [{:keys [devices execution]} (:commands payload)]
                                            (for [{:keys [id]} devices
                                                  {:keys [command params]} execution]
                                              (action @actions id command params))))]

                                (log/debug "Execution:" (seq cmds))
                                
                                (let [results (doall (->> cmds
                                                          (map (fn [{:keys [device] :as cmd}]
                                                                 (Thread/sleep 1000)
                                                                 (log/debug "Command:" cmd)
                                                                 [device   (when-let [port (-> @options :ir)]
                                                                             (with-ir-port (ir-open port) (ir-play (-> cmd :code))))]))))
                                      results-response [{:ids (map first (filter last results))
                                                         :status "SUCCESS"}
                                                        {:ids (map first (filter #(not (last %)) results))
                                                         :status "ERROR"}]]
                                  
                                  (log/debug results-response)
                                  
                                  {:requestId (str requestId)
                                   :payload {:commands results-response}}))
      (do (log/error "Error:" req)
          {:requestId requestId
           :payload { :errorCode "unknownError"}}))))



(defn- wrap-dir-index [handler]
  (fn [req]
    (handler
     (let [req-modified (-> req
                            (update-in [:uri] #(if (= "/" %) "/index.html" %))
                            (update-in [:path-info]  #(if (= "/" %) "/index.html" %)))]
       req-modified))))


(defmulti handle-api (fn [data]
                       (keyword (s/lower-case (or (name (:fn data)) "none")))))



(defmethod handle-api :devices [data]
  (log/debug "devices: " data)
  (assoc u/success :devices @devices)
  ;; (assoc u/success :devices (map-indexed (fn [i {:keys [path host port name]}]
  ;;                                          (let [x (or path (str host ":" port))]
  ;;                                            {:name name :tags (if (= i 0) ["default"] []) :did x}))
  ;;                                        @devices))
  )


(defmethod handle-api :study [{f :fn did :did tags :tags :as data}]
  (log/debug "study starts with: " data)
  (when-let [port (-> @options :ir)]
    (try
      (with-ir-port (ir-open port) (ir-capture))
      (assoc u/success :did default-did)
      (catch Exception e  (log/error (.getMessage e)) (assoc u/fail :did default-did)))))

(defmethod handle-api :play [{f :fn did :did tags :tags ircode :ircode :as data}]
  (log/debug "play starts with: " data)
  (when-let [port (-> @options :ir)]
    (log/debug "ircode: " ircode)
    (try
      (with-ir-port (ir-open port) (ir-play ircode))
      (assoc u/success :did default-did)
      (catch Exception e (assoc u/fail :did default-did)))))






(defmethod handle-api :sync [{f :fn target :target acts :acts read-only :read-only :as data}]
  (log/debug "sync starts: " data)
  (try
    (when-not read-only
      (reset! actions (vec acts))
      (spit (:actions @options) @actions))
    (assoc u/success :ops @actions)
    (catch Exception e (log/error e) (assoc u/fail))))



(defmethod handle-api :pull [{f :fn did :did tags :tags :as data}]
  (log/debug "pull starts with: " data)
  (when-let [port (-> @options :ir)]
    (try
      (assoc u/success :ircodes [(assoc u/success
                                        :did default-did
                                        :result (u/json-> (with-ir-port (ir-open port) (ir-save))))])
      (catch Exception e (log/error (.getMessage e)) (assoc u/fail :did default-did)))))

(defmethod handle-api :bridger [{f :fn did :did tags :tags :as data}]
  (log/debug "bridger starts with: " data)
  (assoc u/success  :version config/+version+))




(defmethod handle-api :action-sync-request [_]
  (let [aid (agent-user-id)]
    (log/info "Action Sync Request for " aid)
    ;; Temporary to pass synq request from Bridger (< 35 (count aid))
    (if-let [sync-key (:homegraph-key @options) ] 
      (let [{:keys [status body] :as result} @(http/post "https://homegraph.googleapis.com/v1/devices:requestSync"
                                                  {:query-params {:key sync-key}
                                                   :headers {"Content-Type" "application/json"}
                                                   :body (u/json<- {:agent_user_id aid})})
            {:keys [error] :as result} (u/json-> body)]

        (if error
          (assoc u/fail :msg (-> error :message))
          u/success))
      (assoc u/success :msg "Invalid agent id"))))


(def api-headers {"Content-Type" "application/json"
                  "X-XSS-Protection" "1; mode=block"
                  "X-Content-Type-Options" "nosniff"
                  "X-Frame-Options" "DENY"
                  "Access-Control-Allow-Methods" "POST"
                  "Access-Control-Allow-Headers" "Content-Type"})



(defn bootstrap [& [opts]]
  (log/info "Core Boot: " opts)
  (let [{oauth? :oauth a-file :actions s-file :secrets h-file :homegraph-key host :host port :port root-path :path} opts
        loader (->> (Thread/currentThread) (.getContextClassLoader))
        listen-conf {:host (or host "0.0.0.0") :port (or port 10001)}
        intra-conf (update listen-conf :port inc)]

    (if-let [dev (:ir opts)]
      (reset! devices (let [[host port] (s/split dev #":")
                            name "irMagician-0"]
                        [(if port
                           {:name name :did (str host ":" port) :host host :port port :tags ["default"]}
                           {:name name :did dev :path dev :tags ["default"]})]))
      (let [devs (u/device-scan)]
        (reset! devices (if-not (some #(contains? (:tags %) "default") devs)
                          (cons (update (first devs) :tags conj "default") (rest devs))
                          devs))

          (doseq [{:keys [path host port]} @devices]
            (log/info "irMagician Found: " (or path (str host ":" port))))))
    
    (reset! options opts)
    
    (try
      (if a-file
        (do (log/info "Loading: " a-file)
            (reset! actions (read-string (slurp a-file)))
            (swap! options assoc :actions a-file))
        (swap! options assoc :actions "actions.edn"))
      (catch Exception e (log/error (str "Load failed: " a-file))))

    (try
      (when s-file
        (log/info "Loading: " s-file)
        (swap! options assoc :ukey (s/trim (slurp s-file))))
      (catch Exception e (log/error (str "Load failed: " s-file))))

    (try
      (when h-file
        (log/info "Loading: " h-file "for Homegraph update")
        (swap! options assoc :homegraph-key (s/trim (slurp h-file))))
      (catch Exception e (log/error (str "Load failed: " h-file))))
    

    (log/info "UI Boot: " intra-conf)
    (boot-on-container (-> (fn [req] {:status 200
                                      :body "OK"})
                           (wrap-resource "/ui" {:loader loader
                                                 :headers api-headers})
                           (wrap-dir-index))
                       (conj {:path (str root-path "/")} intra-conf))
    
    (boot-on-container (-> (fn [{:keys [headers body] :as req}]
                             (conj {:status 200
                                    :headers api-headers}
                                   (let [body (slurp body)]
                                     (log/debug "Body: " body)
                                     (when (seq body) {:body (u/json<- (handle-api (u/json-> body)))}))))
                           (wrap-defaults api-defaults))
                       (conj {:path (str root-path "/api/v1/call")} intra-conf))
    
    
    (boot-on-container (-> (fn [{:keys [headers] :as req}]
                             
                             (when (or (not oauth?) (and oauth? (oauth/client :access_token (last (s/split (headers "authorization") #"[ ]+")))) true) 
                               (let [{:keys [payload] :as result}  (handle-action req)]
                                 (log/debug "action called: " result)
                                 (conj {:headers api-headers}
                                       (if-let [error  (:errorCode payload)]
                                         {:status 400 :body (u/json<- {:msg "Error occurred" :error error})}
                                         {:status 200 :body (u/json<- result)})))))
                           (wrap-defaults api-defaults))
                       (conj {:path (str root-path "/action/v2/call")} listen-conf))

    (when oauth?
      (oauth/init (select-keys @options [:ukey]))
      (boot-on-container oauth/server (conj {:path (str root-path "/oauth")} listen-conf)))

    (log/info "Servers ready")))



;; REQ: {fn: "devices"}
;; RES: {"status":0,"devices":[{"name":"eRemote","lanaddr":"192.168.11.36@80","type":10039,"did":"0000000000000000000034ea34f5dc64","tags":["default"]}]}

;; REQ: {fn: "bridger", read-only: true}
;; RES: {"ip":"192.168.11.39","dns1":"192.168.11.2","lease":172800,"ssid":"\"sid\"","cloud":false,"dns2":"192.168.11.1","gateway":"192.168.11.1","status":0,"id":"c6039d2c-99cc-4dd2-89e7-d2174cf5465f","netmask":"255.255.255.0","version":"0.9.2-SNAPSHOT"}

;; REQ: {fn: "sync", target: "operations", read-only: true}
;;

;; REQ:  {fn: "sync", target: "actions", read-only: true}


;; REQ: {fn: "study", tags: ["default"]}
;; RES: [{"did":"0000000000000000000034ea34f5dc64","status":0,"result":true,"message":"success"}]

;; REQ: {fn: "pull", tags: ["default"]}
;; RES: {"status":0,"ircodes":[{"did":"0000000000000000000034ea34f5dc64","status":0,"result":{"idx":1,"val":"260032016f380d0e0e2a0d0e0e2a0d0e0e290f0d0e290e0e0d2a0d0f0d2a0d2a0d0f0d2a0d0f0d2a0e290d2a0d2a0d0f0d0f0d0f0d2a0e0d0e2a0d0e0e0e0d2a0e0e0d0f0d0f0d0f0d2a0e290d0f0d2a0d0f0d0f0d0e0f290d0e0e0e0d0f0d2a0e0e0d2a0d2a0d0009076e380e0e0f280f0d0d2a0e0e0d2a0e0e0d2a0e0a112a0d0f0d2a0d2a0d0f0d2a0d0f0e290e290d2a0d2a0d0b120d0e0b11290f0d0e290e0e0e0e0d2a0e0a110f0d0f0e0e0d2a0e290e0e0e290d0f0d0e0e0e0e290e0e0d0f0e0e0d2a0e0a112a0e290e0009066e380e0a12290e0a112a0e0e0d2a0e0814290e0d0f280f0d0f280e290f0d0e290f0d0e290e290e290f280f0d0e0e0e0e0d2a0e0e0d2a0d09140d0e2a0d0e0e09120f0e0e0e290f280f0d0e290e0e0e0b100f0d2a0d0f0d0e0e0e0e290e0e0d2a0d2a0e000d05000000000000"},"message":"success"}]}

;; REQ: {fn: "play", tags: ["default"], ircode:"{\"format\": \"raw\"....}" }

;; REQ: {fn: "update-device-info", did: "0000000000000000000034ea34f5dc64", name: "eRemote", tags: ["office","default"], default: true}
;; RES: {"status":0}


;; REQ: {fn: "sync", target: "actions", read-only: true}
;; RES: {"status":0,"ops":[{"name":"AQUOS","category":"LIGHT","controller":"default","code":{"format":"raw","freq":38,"data":[74,51,25,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,18,6,6,6,18,6,6,6,18,6,18,6,18,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,6,6,6,6,18,6,6,6,6,6,6,6,6,6,18,6,18,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,18,6,153,51,25,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,18,6,6,6,18,6,6,6,18,6,18,6,18,6,18,6,6,7,5,7,5,7,18,7,5,7,18,7,5,7,5,7,18,7,5,7,5,7,5,7,5,7,18,7,18,7,5,7,18,7,5,7,5,7,5,7,18,7,5,7,5,7,5,7,18,7,5,7,18,7,18,7,152,51,25,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,18,6,6,6,18,6,6,6,18,6,18,6,18,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,6,6,6,6,18,6,6,6,6,6,6,6,6,6,18,6,18,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,18,6],"postscale":100},"interface":{"vendor":"google","id":"action.devices.traits.OnOff","display":"TurnOn","params":{"on":true}},"id":"f44e804b-32c6-43aa-b738-db6944635df5"},{"name":"AQUOS","category":"LIGHT","controller":"default","code":{"format":"raw","freq":38,"data":[74,51,25,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,18,6,6,6,18,6,6,6,18,6,18,6,18,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,6,6,6,6,18,6,6,6,6,6,6,6,6,6,18,6,18,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,18,6,153,51,25,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,18,6,6,6,18,6,6,6,18,6,18,6,18,6,18,6,6,7,5,7,5,7,18,7,5,7,18,7,5,7,5,7,18,7,5,7,5,7,5,7,5,7,18,7,18,7,5,7,18,7,5,7,5,7,5,7,18,7,5,7,5,7,5,7,18,7,5,7,18,7,18,7,152,51,25,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,18,6,6,6,18,6,6,6,18,6,18,6,18,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,6,6,6,6,18,6,6,6,6,6,6,6,6,6,18,6,18,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,18,6],"postscale":100},"interface":{"vendor":"google","id":"action.devices.traits.OnOff","display":"TurnOff","params":{"on":false}},"id":"f44e804b-32c6-43aa-b738-db6944635df5"},{"name":"abcde","category":"TV","controller":"default","code":{"format":"raw","freq":38,"data":[210,1,236,51,25,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,18,6,6,6,18,6,6,6,18,6,18,6,18,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,6,6,6,6,18,6,6,6,6,6,6,6,6,6,6,6,18,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,6,6,6,6,18,6,18,6,18,6,18,6,152,51,25,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,18,6,6,6,18,6,6,6,18,6,18,6,18,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,6,6,6,6,18,6,6,6,6,6,6,6,6,6,6,6,18,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,6,6,6,6,18,6,18,6,18,6,18,6,152,51,25,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,6,6,18,6,18,6,6,6,18,6,6,6,18,6,18,6,18,6,18,6,6,6,6,6,6,6,18,6,6,6,18,6,6,6,6,6,18,6,6,6,6,6,6,6,6,6,6,6,18,6,6,6,18,6,6,6,6,6,6,6,18,6,6,6,6,6,6,6,18,6,18,6,18,6,18,6],"postscale":100},"interface":{"vendor":"google","id":"action.devices.traits.OnOff","display":"TurnOn","params":{"on":true}},"id":"9b839e3c-4a44-48d1-929e-bfc95a54c9e2"}]}



;; REQ: {fn: "sync", target: "actions", acts: [{name: "vol+1", category: "TV", controller: "default",…}]}

;; REQ: {fn: "cloud-sync"}
;; RES: {"status":0,"msg":"success"}
