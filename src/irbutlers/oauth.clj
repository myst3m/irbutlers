(ns irbutlers.oauth
  (:require [clojure.data.codec.base64 :as b64]
            [clojure.string :as s]
            [clojure.data.json :as json]
            [compojure.core :refer :all]
            [taoensso.timbre :as log]
            [ring.middleware.defaults :refer [api-defaults wrap-defaults]])
    (:require [hiccup.core :as hc])
  (:require [silvur.datetime :refer :all]
            [irbutlers.util :refer :all]))

;; Default 
(def authorized-users (atom #{}))

(defonce clients (atom {}))
(def oauth-client {:client-id "irene" :client-secret "irene"} )


(defn init [{:keys [ukey]}]
  (try (swap! authorized-users conj {:uid "private-use" :ukey ukey})
       (log/info "Init OAuth server")
       (log/debug @clients)
       (catch Exception e nil)))

(defn client [k v]
  (try
    (->> @clients
         (filter #(and (= (k (last %)) v) (< (datetime*) (+ (* 1000 (:expires_in (last %))) (:updated (last %)))) ))
         (first))
    (catch Exception e (log/error "Invalid data in clients"))))


(defn -random-token []
  (s/join (map char (b64/encode (.getBytes (str (java.util.UUID/randomUUID)))))))

(defn authorize [{:keys [params] :as req}]
  (let [{:keys [response_type client_id redirect_uri scope state]} params
        {:keys [uri]} req
        code (-random-token)]
    (log/info "authorize: " req)
    {:status 200
     :headers {"Content-Type" "text/html"}
     :body 
     (hc/html [:html
               [:head
                [:meta {:name "viewport" :content "width=device-width, initial-scale=1, shrink-to-fit=no"}]
                [:link {:rel "stylesheet" :href "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"}]]
               [:body
                [:p]
                [:div.jumbotron.text-center {:style "color:white; background:rgba(0,128,128, 0.7); "}
                 [:h1 "Irene"]
                 [:p "irMagician controller for private use"]]
                [:div.container
                 [:div.d-flex.justify-content-center
                  [:form {:action (s/replace uri #"(.*)/(.*)$" "$1/callback")}
                   [:input {:style "border-radius: 8px;"
                            :placeholder "Put your token" :type "text" :name "token"}]
                   [:p]
                   [:input {:type "hidden" :name "code" :value code}]
                   [:input {:type "hidden" :name "redirect_uri" :value redirect_uri}]
                   [:input {:type "hidden" :name "state" :value state}]
                   [:p]
                   [:div.d-flex.justify-content-center
                    [:button.btn.btn-info {:style "border-radius: 8px;"
                              :type "submit"} "Register"]]]]]
                [:script {:src "https://code.jquery.com/jquery-3.2.1.slim.min.js"}]
                [:script {:src "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"}]] ])}))

(defn callback [{:keys [params headers] :as req}]
  (log/info "Authorized user:" @authorized-users)  
  (let [{:keys [token code state redirect_uri]} params
        user (first (filter #(and (= (:ukey %) token)) @authorized-users))]
    
    (log/debug "Given token: " token)
    (log/debug "Clients: " @clients)
    (log/debug "Found user: " user)
    (if user
      (do (swap! clients assoc (keyword code) {:access_token (-random-token)
                                               :expires_in 120
                                               :updated (datetime*)})
          
          {:status 302
           :headers {"Location" (str redirect_uri "?" "code=" code "&" "state=" state)}})

      {:status 401
       :body "Unauthorized"})))

(defn token [{:keys [params] :as req}]
  (log/info "token: " req)
  (let [{:keys [grant_type client_id client_secret refresh_token code]} params
        client (@clients (keyword code))
        grant_type (keyword grant_type)]

    (log/debug "client: " client)
    (log/debug "oauth-client: " oauth-client  (= oauth-client {:client-id client_id :client-secret client_secret}))
    
    
    (cond
      (and client
           (= oauth-client {:client-id client_id :client-secret client_secret})
           (= grant_type :authorization_code))
      (let [v (conj {:refresh_token (-random-token)} client)]
        (log/debug "grant_type: " grant_type )
        (swap! clients dissoc (keyword code))
        (swap! clients assoc (keyword (:refresh_token v)) v)
        {:status 200
         :headers {"Content-Type" "application/json"}       
         :body (json<- (conj {:token_type "bearer"} (select-keys v [:access_token :refresh_token :expires_in])))})

      
      (= grant_type :refresh_token)
      (do
        (log/debug "grant_type: " grant_type )
        (swap! clients assoc-in [(keyword refresh_token) :updated] (datetime*))
        {:status 200
         :headers {"Content-Type" "application/json"}       
         :body (json<- (conj {:token_type "bearer"} (select-keys (@clients (keyword refresh_token)) [:access_token :expires_in])))})
      
      :else
      (do (log/error "auth error: " {:client-id client_id :client-secret client_secret})
          {:status 400
           :headers {"Content-Type" "application/json"}       
           :body (json<- {:error "invalid_grant"})}))))


(defroutes oauth-routes
  (GET  "/auth" [] authorize)
  (GET  "/callback" [] callback)
  (POST "/token" [] token))

(def server
  (wrap-defaults 
   (routes oauth-routes)
   api-defaults))


