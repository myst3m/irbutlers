(ns irbutlers.core
  (:gen-class)
  (:require [clojure.string :as str]
            [irmagician.core :refer [set-default-ir-port!
                                     ir-capture-loop
                                     with-ir-port
                                     ir-open
                                     ir-close!
                                     ir-restore
                                     ir-play
                                     ir-capture
                                     ir-save
                                     ir-version
                                     ir-target-device
                                     ir-bank-memory-seq
                                     ir-dump-in-bank
                                     ir-data-length
                                     ir-postscale]]
            
            [clojure.tools.cli :refer [parse-opts]]
            [taoensso.timbre :as log]
            [irbutlers.alissia]
            [irbutlers.irene]
            [irbutlers.util :as u]
            [bottify.container :as c]))

(def cli-options
  [["-i" "--ir HOST:PORT | DEVICE" "Serial Port to control irMagician"
    :parse-fn (fn [x]
                (let [[host-or-dev port] (str/split x #":")]
                  (if port
                    {:host host-or-dev :port (Long/parseLong port)}
                    {:path host-or-dev})))]
   [nil "--scan" "Scan irMagician on this host"]
   ["-a" "--actions ACTIONS" "Action list"]
   [nil "--homegraph-key FILE" "Homegraph API key"]
   ["-o" "--oauth" "Boot OAuth2 server"]
   ["-C" "--capture-mode" "Capture mode. This option requires arguments as file names to save"]
   ["-X" "--command-mode" "Command mode. Send data written in a given file"]
   ["-R" "--repl-mode" "REPL mode"]])


(defn usage [summary]
  (str "\n"
       "Usage:"
       "\n"
       "  butler: " "\n"
       "    java -jar irbutlers.jar [options] <butler ex. irbutlers.alissia>" "\n"
       "  capture-mode: " "\n"
       "    java -jar irbutlers.jar [options] -c a.json b.json ..." "\n"
       "  command-mode:" "\n"
       "    java -jar irbutlers.jar [options] -x a.json" "\n"
       "  repl-mode:" "\n"
       "    java -jar irbutlers.jar [options] -R "
       "\n"
       "\n"
       "Examples:"
       "\n"
       " - Run irbutlers.irene with OAuth2 server on 10001/tcp for Google Home"
       "\n"
       "   $ java -jar irbutlers.jar -o -i /dev/ttyACM0 -s irene.secrets -p 10001 --homegraph-key homegraph.secrets irbutlers.irene"
       "\n"
       "\n"
       summary))


(defn -main [& args]
  (log/set-level! :error)
  (.addShutdownHook (Runtime/getRuntime) (Thread. c/stop-container))
  (let [{:keys [options arguments summary]} (c/parse-args args cli-options)]
    (cond
      (:capture-mode options) (if-not (empty? arguments)
                                (with-ir-port (ir-open (:ir options))
                                  (ir-capture-loop "captured" arguments))
                                (println summary))
      (:command-mode options) (if-not (empty? arguments)
                                (with-ir-port (ir-open (:ir options))
                                  (doseq [c arguments] (ir-play c)))
                                (println summary))
      (:repl-mode options) (with-ir-port (ir-open (:ir options))
                             (binding [*ns* (the-ns 'irbutlers.core)]
                               (clojure.main/repl)))
      (:scan options) (doseq [dev (u/device-scan)]
                        (log/info "Found irMagician: " dev))
      
      :else (if (empty? arguments)
              (println (usage summary))
              (do (log/set-level! (:log-level options))
                  (log/debug "Options:" args options)
                  (c/start-container args cli-options))))))


(defn alissia []
  (-main "-d" "debug"
         "-s" "sofia.secrets"
         ;; "-M" "mongo://192.168.11.2/line"
         "irbutlers.alissia"))


(defn irene []
  (-main "-d" "debug"
;;         "-i" "berry:9999"
         "-o"
         "--homegraph-key" "homegraph.secrets"
         "-p" "10001"
         "-s" "irene.secrets"
         "irbutlers.irene"
         ))
