(ns irbutlers.alissia
  (:gen-class)
  (:require [clojure.data.json :as json])
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.string :as s]
            [clojure.core.async :as async]
            [irmagician.core :refer [with-ir-port ir-open ir-close! ir-restore ir-play ir-capture ir-save ir-version ir-target-device]]
            [bottify.container :refer [container boot-on-container stop-container]]
            [bottify.alexa.skill :as skill]
            [silvur.datetime :refer :all]
            [taoensso.timbre :as log]
            [bottify.line.bot :as bot]
            [immutant.web :as web]
            [immutant.web.async :as sock])
  (:import [com.amazon.speech.speechlet Speechlet IntentRequest LaunchRequest SessionEndedRequest SessionStartedRequest]
           [com.amazon.speech.ui SimpleCard PlainTextOutputSpeech]
           [com.amazon.speech.speechlet SpeechletResponse]
           [com.amazon.speech.speechlet.servlet SpeechletServlet]) )


;; Custom skill

(defn do-handle [dispatch-key session opts])
(def options (atom {}))

(defmulti do-intent (fn [req _ _] (-> req (.getIntent) (.getName) keyword)))

(defmethod do-intent :Mute [req session opts]
  (let [intent (-> req (.getIntent))
        slot (-> intent (.getSlot "instruments"))
        target (-> slot (.getValue))]

    (if-not target
      (str "na ni wo mute ni su ru ka wa ka ri ma sen de shi ta")
      (binding []
        (do-handle (str "Mute-" target) nil opts)
        (str target " wo mute shi ma shi ta")))))

(defmethod do-intent :TurnOn [req session opts]
  (let [intent (-> req (.getIntent))
        slot (-> intent (.getSlot "instruments"))
        target (-> slot (.getValue))]
    (if-not target
      (str "na ni wo tsu ke ru ka wa ka ri ma se n de shi ta")
      (do
        (do-handle (str "TurnOn-" target) nil opts)
        (str target " wo tsu ke ma shi ta")))))

(defmethod do-intent :TurnOff [req session opts]
  (let [intent (-> req (.getIntent))
        slot (-> intent (.getSlot "instruments"))
        target (-> slot (.getValue))]
    (if-not target
      (str "na ni wo ke su ka wa ka ri ma se n de shi ta")
      (do
        (do-handle (str "TurnOff-" target) nil opts)
        (str target " wo ke shi ma shi ta")))))

(defmethod do-intent :ChangeTo [req session opts]
  (let [intent (-> req (.getIntent))
        slot (-> intent (.getSlot "channel"))
        target (-> slot (.getValue))]
    (if-not target
      (str "na ni ni ka e ru ka ki ki ko e ma sen de shi ta")
      (do
        (do-handle (str "ChangeTo-" target) nil opts)
        (str target " ni ka e ma shi ta")))))

(defmethod do-intent :Up [req session opts]
  (let [intent (-> req (.getIntent))
        slot (-> intent (.getSlot "amount"))
        target (-> slot (.getValue))]
    (if-not target
      (str "na ni wo up su ru ka ki ko e ma sen de shi ta")
      (do
        (do-handle (str "Up-" target) nil opts)
        (str target " wo up shi ma shi ta")))))

(defmethod do-intent :Down [req session opts]
  (let [intent (-> req (.getIntent))
        slot (-> intent (.getSlot "amount"))
        target (-> slot (.getValue))]
    (if-not target
      (str "na ni wo down su ru ka ki ko e ma sen de shi ta")
      (do
        (do-handle (str "Down-" target) nil opts)
        (str target " wo down shi ma shi ta")))))

(defmethod do-intent :MapAccountToDevices [req session opts]
  (let [intent (-> req (.getIntent))
        slot (-> intent (.getSlot "id"))
        target (-> slot (.getValue))]
    (if-not target
      (str "na ni wo down su ru ka ki ko e ma sen de shi ta")
      (let [user (-> session (.getUser) (.getUserId))]
        ;; (insert! :maps {:user user :type :alexa :target-type :device :target (str/upper-case target) :t (datetime*)})
        (str target " ni link shi ma shi ta")))))

(defmethod do-intent :default [req session opts]
  (let [intent (-> req (.getIntent))
        slot (-> intent (.getSlot "instruments"))]
    (str "Intent ga waka ri ma sen de shi ta")))



(defn speechlet [opts]
  (proxy [Speechlet][]
    (onSessionStarted [session-started-req session]
      (log/debug "===== OnSessionStarted =====")
      (log/debug session-started-req))
    (onLaunch [launch-req session]
      (log/debug "===== OnLaunch =====")      
      (log/debug launch-req session))
    (onSessionEnded [session-ended-req session]
      (log/debug "===== OnSessionEnded =====")            
      (log/debug session-ended-req session))
    (onIntent [req session]
      (log/debug "===== OnIntent =====")            
      (let [user (-> session (.getUser) (.getUserId))]

        ;; TODO implement
        (let [msg (do-intent req session opts)]
          (SpeechletResponse/newTellResponse (doto (PlainTextOutputSpeech. )
                                               (.setText msg))
                                             (doto (SimpleCard.)
                                               (.setTitle "Card")
                                               (.setContent msg))))))))

(defn bootstrap [opts]
  (let [{s-file :secrets host :host port :port root-path :path} opts
        listen-conf {:host (or host "0.0.0.0") :port (or port 10000)}]
    (try
      (when s-file
        (log/info "Loading: " s-file)
        (swap! options assoc :ukey (s/trim (slurp s-file))))
      (catch Exception e (log/error (str "Load failed: " s-file))))
    
    (boot-on-container (skill/build speechlet opts) (conj {:path (str root-path "/")}))))
